#!/usr/bin/bash

# $1 is expected to hold the path to an environment file holding secrets
source $1

RESTIC=/usr/bin/restic
CACHE_DIR=/var/cache/restic

RESTIC_OPTIONS="--cache-dir=$CACHE_DIR --cleanup-cache"

if [[ $RESTIC_REPOSITORY == b2:* ]]; then
  RESTIC_OPTIONS="$RESTIC_OPTIONS -o b2.connections=50"
fi

systemd-inhibit --why="managing snapshots in $RESTIC_REPOSITORY" \
  $RESTIC forget $RESTIC_OPTIONS \
    --host $(hostname) \
    --tag scheduled \
    --keep-within 8h \
    --keep-last 48 \
    --keep-hourly 96 \
    --keep-daily 90 \
    --keep-weekly 52 \
    --keep-monthly 60 \
    --keep-yearly 75

$RESTIC check $RESTIC_OPTIONS && \
  systemd-inhibit \
    --why="performing housekeeping tasks for $RESTIC_REPOSITORY" \
    $RESTIC prune $RESTIC_OPTIONS
