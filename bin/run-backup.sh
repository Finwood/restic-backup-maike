#!/usr/bin/bash

# $1 is expected to hold the path to an environment file holding secrets
source $1

RESTIC=/usr/bin/restic
CACHE_DIR=/var/cache/restic

RESTIC_OPTIONS="--cache-dir=$CACHE_DIR --cleanup-cache"

if [[ $RESTIC_REPOSITORY == b2:* ]]; then
  RESTIC_OPTIONS="$RESTIC_OPTIONS --option b2.connections=50"
fi

systemd-inhibit --why="backing up data to $RESTIC_REPOSITORY" \
  $RESTIC backup $RESTIC_OPTIONS \
    --tag scheduled \
    --exclude-caches \
    --exclude "**/lost+found" \
    --exclude "**/__pycache__" \
    --exclude "**/.*.un~" \
    --exclude "**/~*" \
    "/home" \
    "/root" \
    "/boot" \
    "/etc" \
    "/usr" \
    "/opt" \
    "/var/lib" \
    "/var/mail"
